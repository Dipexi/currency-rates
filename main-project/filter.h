#ifndef FILTER_H
#define FILTER_H

#include "currency.h"

currency** filter(currency* array[], int size, bool (*check)(currency* element), int& result_size);

bool check_results_by_banksname(currency* element);

bool check_results_by_sale(currency* element);

#endif