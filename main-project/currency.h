#ifndef CURRENCY_H
#define CURRENCY_H

#include "constants.h"


struct currency
{
    char banksname[MAX_STRING_SIZE];
    long double buying;
    long double sale;
    char address[MAX_STRING_SIZE];
};

#endif