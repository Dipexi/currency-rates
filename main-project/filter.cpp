#include "filter.h"
#include <cstring>
#include <iostream>

currency** filter(currency* array[], int size, bool (*check)(currency* element), int& result_size)
{
	currency** result = new currency * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_results_by_banksname(currency* element)
{
	return strcmp(element->banksname, "�����������") == 0;
}

bool check_results_by_sale(currency* element)
{
	return element->sale <= 2.5;
}