#ifndef FILE_READER_H
#define FILE_READER_H

#include "currency.h"

void read(const char* file_name, currency* array[], int& size);

#endif