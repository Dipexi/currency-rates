#include <iostream>
#include <iomanip>

using namespace std;

#include "currency.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �4. ����� �����\n";
    cout << "�����: ������� �������\n\n";
    currency* currencys[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", currencys, size);
        cout << "***** ����� ����� *****\n\n";
        for (int i = 0; i < size; i++)
        {
            /********** ����� ����� **********/
            cout << "����............: ";
            // ����� �������� �����
            cout << currencys[i]->banksname << "\n";
            /********** ����� ��������� ������� � ������� **********/
            cout << "�������.........: ";
            // ����� ��������� �������
            cout << currencys[i]->buying << "\n";
            cout << "�������.........: ";
            // ����� ��������� �������
            cout << currencys[i]->sale << "\n";
            /********** ����� ������ **********/
            cout << "�����...........: ";
            // ����� ������ ��������� �����
            cout << currencys[i]->address << "\n";
            cout << '\n';
        }

        bool (*check_function)(currency*) = NULL; // check_function - ��� ��������� �� �������, ������������ �������� ���� bool,
                                                         // � ����������� � �������� ��������� �������� ���� book_subscription*
        cout << "\n�������� ������ ���������� ��� ��������� ������:\n";
        cout << "1) ����� ������ �� ���� ���������� ����� ������������ (� ��������).\n";
        cout << "2) ����� ������ � ������ ��������� ������, � ������� ������� ������ 2,5.\n";
        cout << "\n������� ����� ���������� ������: ";
        int item;
        cin >> item;
        cout << '\n';
        switch (item)
        {
        case 1:
            check_function = check_results_by_banksname; // ����������� � ��������� �� ������� ��������������� �������
            cout << "***** ����� ������ �� ���� ���������� ����� ������������ (� ��������) *****\n\n";
            break;
        case 2:
            check_function = check_results_by_sale; // ����������� � ��������� �� ������� ��������������� �������
            cout << "***** ����� ������ � ������ ��������� ������, � ������� ������� ������ 2,5 *****\n\n";
            break;
        default:
            throw "������������ ����� ������";
        }
        if (check_function)
        {
            int new_size;
            currency** filtered = filter(currencys, size, check_function, new_size);
            for (int i = 0; i < new_size; i++)
            {
                /********** ����� ����� **********/
                cout << "����............: ";
                // ����� �������� �����
                cout << filtered[i]->banksname << "\n";
                /********** ����� ��������� ������� � ������� **********/
                cout << "�������.........: ";
                // ����� ��������� �������
                cout << filtered[i]->buying << "\n";
                cout << "�������.........: ";
                // ����� ��������� �������
                cout << filtered[i]->sale << "\n";
                /********** ����� ������ **********/
                cout << "�����...........: ";
                // ����� ������ ��������� �����
                cout << filtered[i]->address << "\n";
                cout << '\n';
            }
            delete[] filtered;
        }

        for (int i = 0; i < size; i++)
        {
            delete currencys[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}